#!/bin/env bash

WPDIR="/home/bitnami/stack/wordpress"
WPCONF="${WPDIR}/wp-config.php"
SCRIPTDIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TMPDIR="${SCRIPTDIR}/tmp"
TARNAME="wp_backup.tgz"

## double check our variables
if [ ! -d "${WPDIR}" ]; then echo "'${WPDIR}' does not exist. Please adjust script."; exit 1; fi
if [ ! -f "${WPCONF}" ]; then echo "'${WPCONF}' does not exist. Please adjust script."; exit 1; fi
if ! ls "${SCRIPTDIR}/${0}" >/dev/null 2>&1; then echo "Something went wrong and the script is confused about its location."; exit 1; fi

## remove tmpdir if it exists, then remake it
[ -d "${TMPDIR}" ] && rm -rf "${TMPDIR}" &>/dev/null
mkdir "${TMPDIR}"

## get wp config details
# define( 'DB_NAME', 'VALUE' );
# define( 'DB_USER', 'VALUE' );
# define( 'DB_PASSWORD', 'VALUE' );
# define( 'DB_HOST', 'VALUE' );
conf=$(grep "'DB_" "${WPCONF}")
db_name=$(echo "${conf}" | grep "'DB_NAME'," | cut -d\' -f4)
db_user=$(echo "${conf}" | grep "'DB_USER'," | cut -d\' -f4)
db_pass=$(echo "${conf}" | grep "'DB_PASSWORD'," | cut -d\' -f4)
db_host=$(echo "${conf}" | grep "'DB_HOST'," | cut -d\' -f4)

## check our inputs
if [ "${db_name}x" == "x" ]; then echo "Could not get DB_NAME from '${WPCONF}'."; exit 1; fi
if [ "${db_user}x" == "x" ]; then echo "Could not get DB_USER from '${WPCONF}'."; exit 1; fi
if [ "${db_pass}x" == "x" ]; then echo "Could not get DB_PASSWORD from '${WPCONF}'."; exit 1; fi
if [ "${db_host}x" == "x" ]; then echo "Could not get DB_HOST from '${WPCONF}'."; exit 1; fi

## dump database to tmpdir
export MYSQL_PWD="${db_pass}"
if ! mysqldump -h "${db_host}" -u "${db_user}" "${db_name}" > "${TMPDIR}/${db_name}.sql"; then
  echo "Could not take database backup. host: ${db_host} :: db: ${db_name} :: user: ${db_user}"
  exit 1
fi
unset MYSQL_PWD

## tar up the wordpress directory and the sqldump into a backup file
wantdir=$(basename "${WPDIR}")
tardir=$(dirname "${WPDIR}")
if ! tar -czvf "${TARNAME}" --directory "${tardir}" "${wantdir}" --directory "${TMPDIR}" "${db_name}.sql" &>/dev/null; then
  echo "Could not create backup archive."
  exit 1
fi

echo "${SCRIPTDIR}/${TARNAME}"

## cleanup
rm -rf "${TMPDIR}"
